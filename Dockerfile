ARG DOCKER_TAG

FROM pelso/php-dev:${DOCKER_TAG}

ARG DOCKER_TAG
ARG PHP_VERSION
ARG XDEBUG_VERSION

USER root

RUN apt-get update \
 && apt-get install -y \
    php${PHP_VERSION}-xdebug

COPY xdebug${XDEBUG_VERSION}.ini.template /etc/php/${PHP_VERSION}/mods-available/xdebug.ini
COPY xdebug_setter.sh /home/app/xdebug_setting.sh
RUN chown app:app /home/app/xdebug_setting.sh \
 && chmod 774     /home/app/xdebug_setting.sh \
 && chown root:app /etc/php/${PHP_VERSION}/mods-available/xdebug.ini \
 && chmod 664      /etc/php/${PHP_VERSION}/mods-available/xdebug.ini \
 && cat "/home/app/xdebug_setting.sh" > /home/app/.bashrc

ENV PROXY_SERVER_NAME="php-image-test"
ENV XDEBUG_CLIENT_PORT="9000"
ENV XDEBUG_IDE_KEY="PHPSTORM"
ENV XDEBUG_FORCE_IP_DETECTION="false"
ENV PHP_IDE_CONFIG="serverName=${PROXY_SERVER_NAME}"

USER app
WORKDIR /home/app/docroot

ENV PHP_VERSION=${PHP_VERSION}
ENV XDEBUG_VERSION=${XDEBUG_VERSION}

CMD /home/app/xdebug_setting.sh \
 && sudo /usr/sbin/service php${PHP_VERSION}-fpm start \
 && sleep infinity
