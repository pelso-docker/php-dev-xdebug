# php-dev-xdebug

## This project extends my basic php projects with xdebug support:
* https://gitlab.com/pelso-docker/php-dev
* https://gitlab.com/pelso-docker/php

## IDE Settings
* remote port: `9000`
* ide key: `PHPSTORM`

## xdebug Configuration
### Basic configuration
You can override the following values with environment settings.
* PROXY_SERVER_NAME="php-image-test"
* XDEBUG_CLIENT_PORT="9000"
* XDEBUG_IDE_KEY="PHPSTORM"
### Further customization
To customize completely it to you own requirements, I suggest
to follow the method described in the base image documentation: https://gitlab.com/pelso-docker/php#configuration
But of course, here you need to change the `/etc/php/{PHP_VERSION}/mods-available/xdebug.ini` file, and the
`/home/app/xdebug_setting.sh` .

## Behind the curtains
To able to connect the xdebug to your ide, it needs to be get the docker machine's gateway. To achieve that, on the 
startup, the container runs the `xdebug_setting.sh` which detects and sets the proper ip. The similar thing happens
when a user runs a command inside the container: the `.bashrc` runs the shell script and sets the anvironment variables
to set the environment variables for the cli.

## Php versions & tags, Users & rights, Architectures, Php extensions, Configuration
https://gitlab.com/pelso-docker/php

## XDEBUG
https://xdebug.org/