#!/bin/bash

echo "Setting Xdebug..."

set -x

GATEWAY_IP=$(route -n | grep 'UG[ \t]' | awk '{print $2}')

cp /etc/php/${PHP_VERSION}/mods-available/xdebug.ini ~/xdebug.ini
if [[ "${XDEBUG_FORCE_IP_DETECTION}" == "true" ]]
then
  sed -i "/xdebug\.client_host=/c\xdebug\.client_host=${GATEWAY_IP}" ~/xdebug.ini
  sed -i "/xdebug\.remote_host=/c\xdebug\.remote_host=${GATEWAY_IP}" ~/xdebug.ini
fi
sed -i "s/<CLIENT_PORT>/${XDEBUG_CLIENT_PORT}/g" ~/xdebug.ini
sed -i "s/<IDE_KEY>/${XDEBUG_IDE_KEY}/g" ~/xdebug.ini
cat ~/xdebug.ini > /etc/php/${PHP_VERSION}/mods-available/xdebug.ini
rm  ~/xdebug.ini

export PHP_IDE_CONFIG="serverName=${PROXY_SERVER_NAME}"

if [[ "${XDEBUG_VERSION}" == "3" ]]
then
  export XDEBUG_MODE=debug
  export XDEBUG_SESSION=1
else
  export XDEBUG_CONFIG="remote_enable=1 remote_mode=req remote_host=${GATEWAY_IP} remote_port=${XDEBUG_CLIENT_PORT} idekey=${XDEBUG_IDE_KEY} remote_connect_back=0"
fi

set +x